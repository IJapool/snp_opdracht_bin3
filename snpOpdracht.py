#!/usr/bin/env python3

_author_ = "IJsbrand Pool"
_version_ = 1.1.1

import sys
import argparse
import math

table = {"TTT": "F", "TTC": "F", "TTA": "L", "TTG": "L",
         "TCT": "S", "TCC": "S", "TCA": "S", "TCG": "S",
         "TAT": "Y", "TAC": "Y", "TAA": "-", "TAG": "-",
         "TGT": "C", "TGC": "C", "TGA": "-", "TGG": "W",
         "CTT": "L", "CTC": "L", "CTA": "L", "CTG": "L",
         "CCT": "P", "CCC": "P", "CCA": "P", "CCG": "P",
         "CAT": "H", "CAC": "H", "CAA": "Q", "CAG": "Q",
         "CGT": "R", "CGC": "R", "CGA": "R", "CGG": "R",
         "ATT": "I", "ATC": "I", "ATA": "I", "ATG": "M",
         "ACT": "T", "ACC": "T", "ACA": "T", "ACG": "T",
         "AAT": "N", "AAC": "N", "AAA": "K", "AAG": "K",
         "AGT": "S", "AGC": "S", "AGA": "R", "AGG": "R",
         "GTT": "V", "GTC": "V", "GTA": "V", "GTG": "V",
         "GCT": "A", "GCC": "A", "GCA": "A", "GCG": "A",
         "GAT": "D", "GAC": "D", "GAA": "E", "GAG": "E",
         "GGT": "G", "GGC": "G", "GGA": "G", "GGG": "G"}


def aminoacid_searcher(pos, snp, seq):
    """
    Searches for the aminoacid and triplets for the SNP.
    :param pos: Position of the mutation.
    :param snp: New nucleotide.
    :return:
    """
    sequence = open(seq)
    seq = ""
    for line in sequence:
        if not line.startswith(">"):
            seq += line.strip()
    triplet = seq[pos - (pos % 3):pos + (3 - (pos % 3))]
    triplet_new = list(triplet)
    triplet_new[(pos % 3)] = snp
    triplet_new = ''.join(triplet_new)

    aacid = table[triplet]
    aa_new = table[triplet_new]
    return aacid, aa_new


def msa_reader(msa):
    """
    Reads in the MSA file and returns the full sequences.
    :param msa: The path to the MSA file.
    :return: A dictionary with the headers and full sequences.
    """
    msa_file = open(msa)
    sequences = {}
    for line in msa_file:
        if line.startswith("gi"):
            line = line.strip()
            seqname = line[:30]
            # Either adds the header and the part of the sequence, or extends the existing sequence
            if seqname in sequences:
                sequences[seqname] += line[36:]
            else:
                sequences[seqname] = line[36:]
    return sequences


def compare_proteins(pos, newaa):
    """
    Compares the proteins to calculate the score.
    :param pos: The position of the amino acid.
    :param newaa: The new amino acid.
    :param msa: The multiple sequence alignment.
    :return: A score between 0 and 10, rounded on 2 decimals.
    """
    proteins = []
    scores = {}
    sequences = msa_reader("gene.fasta")

    # Puts all the proteins on location pos in a list
    for protseq in sequences:
        proteins.append(sequences[protseq][pos])

    # Calculates the score for each protein
    for uniqueprot in set(proteins):
        scores[uniqueprot] = proteins.count(uniqueprot) / len(proteins) * 10

    # Returns the score
    if newaa in scores:
        return round(scores[newaa], 2)
    else:
        return 0


def score_message(score):
    """
    Prints the score with a fitting message.
    :param score: The score.
    :return:
    """
    if score >= 7.5:
        print("Mutation is highly conserved on this position, score = ", score)
    elif score >= 5:
        print("Mutation is somewhat conserved on this position, score = ", score)
    elif score >= 2.5:
        print("Mutation is mildly conserved on this position, score = ", score)
    elif score > 0:
        print("Mutation is very little conserved on this position, score = ", score)
    else:
        print("None of the sequences have this amino acid on this position, this is a bad mutation with score 0.")


def argument_parser():
    """
    This function creates an 'argparse' parser and adds arguments.
    :return parser.parse_args(): - The parser arguments
    """
    # Create parser with a description and usage
    parser = argparse.ArgumentParser(
        description='Determines conservation.',
        usage='python3.7 snpopdracht.py -n [Nucleotide] -p [Snp position] -s [Nucleotide sequence]'
    )

    # Add arguments:
    parser.add_argument('-n', '--nuc', type=str, metavar='', required=True,
                        help='SNP Nucleotide')
    parser.add_argument('-p', '--pos', type=int, metavar='', required=True,
                        help='Gene position')
    parser.add_argument('-s', '--seq', type=str, metavar='', required=True,
                        help='Sequence without snp')

    return parser.parse_args()


def main(args):
    """
    :param args:
    :return:
    """
    try:
        aacid, aa_new = aminoacid_searcher(args.pos, args.nuc, args.seq)
        if aacid != aa_new:
            location = math.ceil(args.pos / 3 - 1)
            mutation_score = compare_proteins(location, aa_new)
            score_message(mutation_score)
        else:
            print("Inconsequential: Mutation has no effect. Score is 10")
    except IndexError:
            print("Oops! Something went wrong, this index is out of range please try again")
    except KeyError:
        print("Oops! Something went wrong, please select between the following nucleotides {A, C, T, G}")
    except FileNotFoundError:
        print("Oops! Something went wrong, This file doesnt exist here...")
    return 0


if __name__ == "__main__":
    sys.exit(main(argument_parser()))

