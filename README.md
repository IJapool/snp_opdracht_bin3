READ_ME: snp.py

author: IJsbrand Pool
date: 04-11-2021
files: snpopdracht.py, nucsequence.fasta, gene.fasta

Description of the code:
	The script snpopdracht.py can be used in the commandline with the following arguments:

    -n: This is the nucleotide change select {A, C, T, G}
    -p: The position in the sequence where the SNP needs to be implemented and scored
    -s: The nucleotide sequence file.
	The program snp_annotation.py will handle a few input arguments:

When executed with these arguments, the program will first open the nucleotide sequence file. It Will change the nucleotide
at the given position to the given snp nucleotide. Then it will translate the triplet this nucleotide is part of into the
amino acid. If this new amino acid is the same as the old one, if the snp did not change the amino acid, the program will
return a score of 10 with the message that the mutation has no effect. If the amino acid did change, the program will read
the MSA file and extract all the sequences and their headers in a dictionary. Then it will calculate a score for the amino
acids on the position provided by the user based on the times the amino acid occurs on that position. Then the program will
get the score of the new amino acid on that position and prints a message based on the score.


Usage:
	Open any terminal that can use Python3

	- python3 snpopdracht.py -n [New nucleotide] -p [Snp position] -s [Nucleotide sequence]

Example of proper usage:
	- python3 snpopdracht.py -n "C" -p 57 -s "nucsequence.fasta"

Contact:
  For any questions please contact the email adress below.
  i.j.a.pool@st.hanze.nl